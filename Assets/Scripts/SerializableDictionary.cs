﻿using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;

	
public class SerializableDictionary<TKey,TValue> : Dictionary<TKey,TValue>, IXmlSerializable {
	
	public virtual void WriteXml(XmlWriter writer) {
		XmlSerializerForDictionary.WriteXml(writer, this);
	}//method
	
	public virtual void ReadXml(XmlReader reader) {
		XmlSerializerForDictionary.ReadXml(reader, this);
	}//method
	
	public virtual XmlSchema GetSchema() {
		return null;
	}//method
	
}//class
	

public class XmlSerializerForDictionary {
	
	public struct Pair<TKey,TValue> {
		
		public TKey Key;
		public TValue Value;
		
		public Pair(KeyValuePair<TKey,TValue> pair) {
			Key = pair.Key;
			Value = pair.Value;
		}//method
		
	}//struct
	
	public static void WriteXml<TKey,TValue>(XmlWriter writer, IDictionary<TKey,TValue> dict) {
		
		var list = new List<Pair<TKey,TValue>>(dict.Count);
		
		foreach (var pair in dict) {
			list.Add(new Pair<TKey,TValue>(pair));
		}//foreach
		
		var serializer = new XmlSerializer(list.GetType());
		serializer.Serialize(writer, list);
		
	}//method
	
	public static void ReadXml<TKey, TValue>(XmlReader reader, IDictionary<TKey, TValue> dict) {
		
		reader.Read();
		
		var serializer = new XmlSerializer(typeof(List<Pair<TKey,TValue>>));
		var list = (List<Pair<TKey,TValue>>)serializer.Deserialize(reader);
		
		foreach (var pair in list) {
			dict.Add(pair.Key, pair.Value);
		}//foreach
		
		reader.Read();
		
	}//method
	
}//

