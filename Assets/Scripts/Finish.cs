﻿using UnityEngine;
using System.Collections;

public class Finish : MonoBehaviour 
{
    void OnTriggerEnter2D (Collider2D c)
    {
        Debug.Log("Finished " + c.gameObject.name);
		if (c.gameObject.name == "Finish")
        Player.Instance.Finished();
    }
}
