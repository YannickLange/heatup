﻿using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour 
{
	void OnTriggerEnter2D (Collider2D c)
    {
		Debug.Log("On trigger spike " + c.gameObject.name);
		c.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        Player.Instance.Died();
    }
}
