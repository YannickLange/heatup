using UnityEngine;
using System;
using System.Net;
using System.IO;
using System.Collections;
using SimpleJSON;

public class ServerCall : MonoBehaviour
{
	public JSONNode Response;
	public int Code;

    public ServerCall ()
    {
        
    }
    
	public bool Get (string url)
    {
		return Call("GET", Settings.GetServerUrl () + url);
    }

	public bool Post (string url, JSONNode json)
    {
		return Call("POST", Settings.GetServerUrl () + url, json);
    }

	public bool Put (string url, JSONNode json)
	{
		return Call("PUT", Settings.GetServerUrl () + url, json);
	}

	public bool Call (string type, string url, JSONNode json = null)
	{
		//Debug.Log(type + " to: " + url);

		try {
			WebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(url);
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = type;

			if (json != null) 
			{
				//Debug.Log("Add data: " + json.ToString ());

				using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					streamWriter.Write(json.ToString());
				}
			}

			HttpWebResponse httpResponse = (HttpWebResponse) httpWebRequest.GetResponse();
			Code = (int) httpResponse.StatusCode;
			Response = ReadResponse (httpResponse);
			return true;
		} catch (WebException e) {
			Code = (int) ((HttpWebResponse) e.Response).StatusCode;
			ReadResponse ((HttpWebResponse) e.Response);
			return false;
		}
	}

	private JSONNode ReadResponse (HttpWebResponse httpResponse)
	{
		using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
		{
			string responseText = streamReader.ReadToEnd();
			JSONNode Json = JSONNode.Parse(responseText);
			//if (Json != null) {
				//Debug.Log ("Received data: \t" + Json.ToString());
			//}
			return Json;
		}
	}
}