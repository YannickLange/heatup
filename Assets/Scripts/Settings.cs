﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Globalization;

public class Settings : MonoBehaviour
{
    public static string Domain = "localhost";
	public static string Port = "5984";

    public static string GetServerUrl ()
    {
		return "http://" + Settings.Domain + ":" + Settings.Port;
    }
}
