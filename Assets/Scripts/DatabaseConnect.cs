﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class DatabaseConnect : MonoBehaviour
{
	void Awake ()
	{
		ServerCall call = ServerCaller.Instance.Get ("/");
		if (call.Code != 200)
		{
			Debug.LogError("Can't connect to CouchDB on: " + Settings.GetServerUrl());
			return;
		}
		
		Debug.Log ("Connected to CouchDB on url: " + Settings.GetServerUrl ());
		
		Destroy(call.gameObject);

		call = ServerCaller.Instance.Put ("/heatmap", null);
		if (call.Code != 201 && call.Code == 412)
		{
			Debug.Log ("Database already excists");
			return;
		}
		Debug.Log ("Database created");

		CreateViews();

		Debug.Log ("Views created");

		Destroy(call.gameObject);
	}

	void CreateViews ()
	{
		JSONNode Json = new JSONClass();
		Json["_id"] = "_design/position";
		Json["views"] = new JSONClass();
		Json["views"]["all"] = new JSONClass();
		Json["views"]["all"]["map"] = "function(doc) {if (doc.type == 'Position')emit(doc._id, {x: doc.x, y: doc.y});}";
		Json["views"]["count"] = new JSONClass();
		Json["views"]["count"]["map"] = "function(doc) {if (doc.type == 'Position')emit(doc._id, {x: doc.x, y: doc.y});}";
		Json["views"]["count"]["reduce"] = "function (keys, values, rereduce) { if (rereduce) return sum(values); return values.length; }";
		ServerCall call = ServerCaller.Instance.Put ("/heatmap/_design/position", Json);
		
		Json = new JSONClass();
		Json["_id"] = "_design/death";
		Json["views"] = new JSONClass();
		Json["views"]["all"] = new JSONClass();
		Json["views"]["all"]["map"] = "function(doc) {if (doc.type == 'Death')emit(doc._id, {x: doc.x, y: doc.y});}";
		Json["views"]["count"] = new JSONClass();
		Json["views"]["count"]["map"] = "function(doc) {if (doc.type == 'Death')emit(doc._id, {x: doc.x, y: doc.y});}";
		Json["views"]["count"]["reduce"] = "function (keys, values, rereduce) { if (rereduce) return sum(values); return values.length; }";
		call = ServerCaller.Instance.Put ("/heatmap/_design/death", Json);
	}
}
