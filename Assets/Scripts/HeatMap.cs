﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class HeatMap : MonoBehaviour
{
	public enum Types {Position, Death, None};

	public string url;

	public GameObject HeatMapSprite;

	List<Vector3> Points = new List<Vector3>();

	public Texture2D heatmapImage;

	private static HeatMap _Instance = null;
	public static HeatMap Instance
	{
		get
		{
			if (_Instance == null)
				_Instance = FindObjectOfType<HeatMap>();
			return _Instance;
		}
	}

	public void Start ()
	{
		if (Main.TypeToShow == Types.None) return;

		ServerCall call = ServerCaller.Instance.Get (url + "/_view/all");
		foreach (JSONNode node in call.Response["rows"].AsArray) 
		{
			Points.Add(new Vector3 (node["value"]["x"].AsFloat, node["value"]["y"].AsFloat, 0));
		}

		call = ServerCaller.Instance.Get (url + "/_view/count");
		int PointCount = call.Response["rows"][0]["value"].AsInt;

		// 0.9f is a nice value when there are not that many points. The more
		// points the less sensitive we want to make the alpha detection
		Heatmap.ALPHA_SENSITIVITY = 0.9f - (((PointCount / 300) < 1 ? (PointCount / 300) : 1) * .25f);
		heatmapImage = Heatmap.CreateHeatmap(Points.ToArray(), camera, 25);
		Main.Instance.ChooseHeatmap();
    }

	public void AddPoint (Types type, float x, float y)
	{
		ServerCall call = ServerCaller.Instance.Get ("/_uuids");
		string uuid = call.Response["uuids"].AsArray[0].Value;
		
		JSONNode Json = new JSONClass();
		Json["type"] = new JSONData(type.ToString());
		Json["x"] = new JSONData(x);
		Json["y"] = new JSONData(y);
		call = ServerCaller.Instance.Put ("/heatmap/" + uuid, Json);
		if (call.Code != 201)
		{
			Debug.LogWarning ("Something went wrong while adding point to heatmap");
			return;
		}

		Destroy(call.gameObject);
	}

	public void SpawnHeatMap (float x, float y)
	{
		Instantiate (HeatMapSprite, new Vector3 (x, y, 0), new Quaternion (0, 0, 0, 0));
	}

	public int GetPointCount ()
	{
		return Points.Count;
	}
}
