using UnityEngine;
using System;
using System.Collections;
using SimpleJSON;

public class ServerCaller : MonoBehaviour
{
    private static ServerCaller _Instance = null;
    public static ServerCaller Instance
    {
        get
        {
            if (_Instance == null)
                _Instance = FindObjectOfType<ServerCaller>();
            return _Instance;
        }
    }

    public GameObject ServerCallPrefab;

    public ServerCaller ()
    {

    }
    
	public ServerCall Get (string url)
    {
        ServerCall call = ((GameObject) Instantiate (ServerCallPrefab)).GetComponent<ServerCall>();
		call.Get (url);
		return call;
    }

	public ServerCall Post (string url, JSONNode json)
    {
        ServerCall call = ((GameObject) Instantiate (ServerCallPrefab)).GetComponent<ServerCall>();
		call.Post (url, json);
		return call;
    }

	public ServerCall Put (string url, JSONNode json)
	{
		ServerCall call = ((GameObject) Instantiate (ServerCallPrefab)).GetComponent<ServerCall>();
		call.Put (url, json);
		return call;
	}
}