﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    //Speed for horizontal movement
    public float MovementSpeed;

    //Speed for going up
    public float GoUpSpeed;

    public Camera Cam;
    private float CamWidth;
    private float CamHeight;

    private static Player _Instance = null;
    public static Player Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = FindObjectOfType<Player>();
            }
            return _Instance;
        }
    }

	void Start () 
    {
		InvokeRepeating ("sendPosition", .1f, .1f);

        CamHeight = 2 * Camera.main.orthographicSize;
        CamWidth = CamHeight * Camera.main.aspect;
	}
	
	void Update () 
    {
        Vector3 newposition = transform.right * Input.GetAxis("Horizontal") * MovementSpeed * Time.deltaTime;

        if (newposition.x <= CamWidth ||
            newposition.x >= CamWidth)
        {
            transform.position += newposition;
            transform.position += transform.up * GoUpSpeed * Time.deltaTime;

            Vector3 newCamPos = new Vector3(0.0f, transform.position.y,
                Cam.transform.position.z);
            Cam.transform.position = newCamPos;
        }

		// Restart with R
		if (Input.GetKeyDown (KeyCode.R)) {
			Application.LoadLevel ("GameScene");
		}
    }

    public void Died()
    {
		sendDiePosition ();
		Application.LoadLevel("GameOverScene");
    }

    public void Finished()
    {
        Application.LoadLevel("GameOverScene");
    }

	public void sendPosition ()
	{
		HeatMap.Instance.AddPoint (HeatMap.Types.Position, transform.position.x, transform.position.y);
	}

	public void sendDiePosition ()
	{
		HeatMap.Instance.AddPoint (HeatMap.Types.Death, transform.position.x, transform.position.y);
	}
}
