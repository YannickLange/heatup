﻿using UnityEngine;
using System.Collections;

public class StartScene : MonoBehaviour
{
	public GameObject MainInfo;
	public GameObject DrawingInfo;

	public Button ShowDeaths;
	public Button ShowRoute;
	public Button NoHeatmap;

	void Start ()
	{
		DrawingInfo.SetActive(false);

		ShowDeaths.Click += delegate(GameObject button) {
			Main.TypeToShow = HeatMap.Types.Death;
			MainInfo.SetActive(false);
			DrawingInfo.SetActive(true);
        	Application.LoadLevel("GameScene");
		};

		ShowRoute.Click += delegate(GameObject button) {
			Main.TypeToShow = HeatMap.Types.Position;
			MainInfo.SetActive(false);
			DrawingInfo.SetActive(true);
			Application.LoadLevel("GameScene");
		};

		NoHeatmap.Click += delegate(GameObject button) {
			Main.TypeToShow = HeatMap.Types.None;
			Application.LoadLevel("GameScene");
		};
	}
}
