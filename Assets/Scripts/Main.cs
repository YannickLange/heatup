﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
	public static HeatMap.Types TypeToShow;

	public TextMesh HeatmapText;
	public TextMesh PointsText;

	private static Main _Instance = null;
	public static Main Instance
	{
		get
		{
			if (_Instance == null)
				_Instance = FindObjectOfType<Main>();
			return _Instance;
		}
	}

	public HeatMap HeatMap1;
	public HeatMap HeatMap2;

	public void Start ()
	{
		HeatmapText.text = "No heatmap defined";
		PointsText.text = "";
	}

	public void ChooseHeatmap ()
	{
		if (TypeToShow == HeatMap.Types.Position) {
			Heatmap.CreateRenderPlane(HeatMap1.heatmapImage);
			HeatmapText.text = "Popular Route Heatmap";
			PointsText.text = HeatMap1.GetPointCount() + " Heatmap Points";
		} else {
			Heatmap.CreateRenderPlane(HeatMap2.heatmapImage);
			HeatmapText.text = "Place of Deaths Heatmap";
			PointsText.text = HeatMap2.GetPointCount() + " Heatmap Points";
		}
	}
}
