using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
	public string SceneName = null;

	bool Clicked = false;

	public delegate void EventHandler(GameObject button);
	public event EventHandler Click;
	public virtual void OnClick ()
	{
		// Don't click the button if it doesn't exists anymore
		if (this.gameObject == null) return;

		// One click once when we go to a scene
		if (Clicked) return;

		// Dont do a action if the is not event bind to this button
		if (!HasClickEvent() && string.IsNullOrEmpty(SceneName)) {
			Debug.LogError("Has no click event or scenename");
			return;
		}

		Do ();
	}

	void Start ()
	{
		gameObject.AddComponent<AudioSource>();
	}
	
	void Do ()
	{
		if (Click != null) {
			Click(this.gameObject);
		} else if (!string.IsNullOrEmpty(this.SceneName)) {
			Clicked = true;
			Application.LoadLevel(SceneName);
		}
	}
	
	void OnMouseUp ()
	{
		OnClick();
	}

	public bool HasClickEvent ()
	{
		return Click != null;
	}
}
